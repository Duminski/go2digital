<?php

// A AJOUTER : 
// - isFrFormat : FILTRE SITE EN/FR

/** 
 * Adapte le format de la date en fonction du jour actuel  
 */
function toDynamicDate($date){
    $today = date("d-m-Y");
    $updatedDate = date("d-m-Y", strtotime($date));

    if($today == $updatedDate){
        return 'Aujourd\'hui, ' . date("H:i", strtotime($date));
    }
    else if(yesterday($today, $updatedDate)){
        return 'Hier, ' . date("H:i", strtotime($date));
    }
    // Test si updated d +1 == d
    return date("d/m/Y", strtotime($date)) . ', ' . date("H:i", strtotime($date));
}

/**
 * Teste si la dernière modification était hier
 */
function yesterday($currDate, $updDate){
    $day = substr($updDate,0,2);
    
    if(date('t') == $day){
        $day = 1;
    }
    else{
        $day++;
    }

    if($day == substr($currDate,0,2)){
        return true;
    }
    else{
        return false;
    }
}
?>