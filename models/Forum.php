<?php
Class Forum {
	private $bd;
	private static $instance = null;

	private function __construct(){
		include('../../config.php');
		try{
			$hote='mysql:host=' . $db['host'] . ';dbname=' . $db['name'];
			$user=$db['user'];
			$mdp=$db['pass'];
			$this->bd = new PDO($hote,$user,$mdp);
			$this->bd->query('SET NAMES utf8');
			$this->bd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		}
		catch (PDOExcpetion $e){
			die('<p>Erreur de connexion : ' . $e->getMessage() . '</p>');
		}
	}

	public static function get_model(){
		if(is_null(self::$instance)) {
			self::$instance = new Forum();
		}
		return self::$instance;
	}

	public function getTopics(){
		try {
			$query = $this->bd->prepare('SELECT * FROM topic;');
		 	$query->execute();
		 	$topics =  $query->fetchAll(PDO::FETCH_ASSOC);
		 	return $topics;
		}
		catch (PDOExcpetion $e){
			die('<p>Erreur de requête : ' . $e->getMessage() . '</p>');
		}
    }
    
    public function getNbPosts($topID){
		try {
			$query = $this->bd->prepare('SELECT COUNT(*) FROM post WHERE top_id=:topID;');	
			$query->bindValue(':topID',$topID);
		 	$query->execute();
		 	$nbPosts =  $query->fetch(PDO::FETCH_NUM);
		 	return $nbPosts;
		}
		catch (PDOExcpetion $e){
			die('<p>Erreur de requête : ' . $e->getMessage() . '</p>');
		}
	}

}
?>