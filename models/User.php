<?php
Class User {
	private $bd;
	private static $instance = null;

	private function __construct(){
		include('../../config.php');
		try{
			$hote='mysql:host=' . $db['host'] . ';dbname=' . $db['name'];
			$user=$db['user'];
			$mdp=$db['pass'];
			$this->bd = new PDO($hote,$user,$mdp);
			$this->bd->query('SET NAMES utf8');
			$this->bd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		}
		catch (PDOExcpetion $e){
			die('<p>Erreur de connexion : ' . $e->getMessage() . '</p>');
		}
	}

	public static function get_model(){
		if(is_null(self::$instance)) {
			self::$instance = new User();
		}
		return self::$instance;
	}

	/*public function getUserInfo($username){
		try {
			$query = $this->mysqli->prepare('SELECT COUNT(*) FROM user');
			$query->bindValue(':username',$username);
		 	$query->execute();
		 	$user =  $query->fetchAll(PDO::FETCH_ASSOC);
		 	return $user;
		}
		catch (PDOExcpetion $e){
			die('<p>Erreur de requête : ' . $e->getMessage() . '</p>');
		}
    }*/
    
    public function getIsAdmin($username){
		try {
			$query = $this->bd->prepare('SELECT COUNT(*) FROM post WHERE use_pseudo=:username;');	
			$query->bindValue(':username',$username);
		 	$query->execute();
		 	$isAdmin =  $query->fetch(PDO::FETCH_NUM);
		 	return $isAdmin;
		}
		catch (PDOExcpetion $e){
			die('<p>Erreur de requête : ' . $e->getMessage() . '</p>');
		}
	}

}
?>