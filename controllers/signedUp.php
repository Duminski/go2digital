<?php 
$title = 'Espace personnel';
$linkCss = "/public/css/main.css";
$linkCss2 = "/public/css/login.css";
$linkIcon = "/public/img/logo.ico";
$linkLogo = "/public/img/logo_company.png";
$linkScript = "/public/js/navigation.js";
$linkScript2 = "/public/js/login.js";
ob_start();
?>
<?php
if(!isset($_COOKIE["auth_session"])){
    require('../models/auth.class.php');

$user = new Auth();

$user->register($_POST['numero'], $_POST['email'], $_POST['username'], $_POST['password'], $_POST['confirmPassword']);

?>
    <div class="container">
        <img src="/public/img/back_login.jpg" alt="background_landscape">
        <div class="card" style="height: 80vh">
            <h1 class="title">Inscription</h1>
            <form method="post" action="/controllers/profilePage.php">
                <p style="text-align: center;">Vous êtes bien inscrit ! Un mail de confirmation vous a été envoyé, veuillez cliquer sur le lien qu'il contient pour activer votre compte.</p>
                <br />
                <br />
                <br />
                
            </form>
        </div>
    </div>
<?php
}
else echo 'ok';
?>
<?php 
$content = ob_get_clean(); 
require('../views/template.php'); 
?>