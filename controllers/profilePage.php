<?php
require('../models/auth.class.php');

$user = new Auth();

// If User not connected
if (!isset($_COOKIE['auth_session'])) {
    // Search if the user can be connected
    if(!($user->login($_POST['username'], $_POST['password']))){
        require('../views/loginForm.php');
    } else {
        require('../views/userHome.php');
    }
}

// If user connected
if (isset($_COOKIE['auth_session'])) {
    require('../views/userHome.php');
}

?>