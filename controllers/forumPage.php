<?php
require('../models/Forum.php');
require('../models/User.php');
require('../models/dates.php');

$forum = Forum::get_model();
$user = User::get_model();

$topics = $forum->getTopics();

$topicsToHtml = array();
$tmpArr = array();
foreach($topics as $val){
    $tmpArr['title'] = htmlspecialchars($val['top_name']);
    $tmpArr['desc'] = htmlspecialchars($val['top_desc']);
    $tmpArr['author'] = htmlspecialchars($val['use_pseudo']);
    $tmpArr['updated'] = toDynamicDate(htmlspecialchars($val['top_last_update']));
    $tmpArr['nbPosts'] = htmlspecialchars($forum->getNbPosts($val['top_id'])[0]);

    array_push($topicsToHtml, $tmpArr);
}

require('../views/forumHome.php');
// redirect = header("Location: ../views/forumHome.php")
?>