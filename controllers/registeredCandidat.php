<?php 
$title = 'Espace personnel';
$linkCss = "/public/css/main.css";
$linkCss2 = "/public/css/login.css";
$linkIcon = "/public/img/logo.ico";
$linkLogo = "/public/img/logo_company.png";
$linkScript = "/public/js/navigation.js";
$linkScript2 = "/public/js/login.js";
ob_start(); 
?>
<?php
if(!isset($_COOKIE["auth_session"])){
    $prenom = $_POST['prenom'];
    $nom = $_POST['nom'];
    $email = $_POST['email'];
    print("<center>Bonjour $prenom $nom</center>");
?>
    <div class="container">
        <img src="/public/img/back_login.jpg" alt="background_landscape">
        <div class="card" style="height: 80vh">
            <h1 class="title">Inscription d'un candidat</h1>
            <form method="post" action="/controllers/profilePage.php">
                <p style="text-align: center;">Le candidat a bien été enregistré !</p>
                <br />
                <br />
                <br />
                <p style="text-align: center;">Que voulez-vous faire ?</p>
                <p style="text-align: center;">Enregistrer un autre candidat :</p>
                <div class="button-container">
                    <a href="../views/registerCandidat.php"><input class="button" type="button" value="Inscrire un client"></a>
                </div>
                <p style="text-align: center;">Retourner à votre espace personnel :</p>
                <div class="button-container">
                    <a href="./profilePage.php"><input class="button" type="button" value="Mon espace"></a>
                </div>
            </form>
        </div>
    </div>
<?php
}
else echo 'ok';
?>
<?php 
$content = ob_get_clean(); 
require('../views/template.php'); 
?>