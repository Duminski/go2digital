<?php 
$title = 'G2D';
$linkCss = "./public/css/main.css";
$linkCss2 = "";
$linkCss3 = "";
$linkIcon = "./public/img/logo.ico";
$linkLogo = "./public/img/logo_company.png";
$linkScript = "./public/js/navigation.js";
ob_start(); 
?>
    
    <div class="container" id="home">
        <img src="./public/img/landscape4.jpg" alt="background_landscape">
        <h2 class="moto">Bienvenue</h2>

        <div class="scroll">
            <p>Défilez vers le bas</p>
            <img class="arrow bounce" src="./public/img/down-arrow.png" alt="flèche bas">
        </div>
    </div>

    <div class="header" id="products" style="
    text-align: center;
">
            <h2 id="title" style="
    font-size: 40px;
">Notre équipe : Go 2 Digital</h2>
        </div>
<div class="container" style="display: flex;height: auto;margin-bottom: 5%; flex-direction: row;background: #FFFFFF;">
    <div class="one" style="flex: 0 0 50%;">
     <div class="box_2" style="
    width: auto;
    height: auto;
    box-shadow: 0px 0px 14px 3px #cfcfcf;
    margin-left: 25%;
">
    
    <img src="https://asigma.fr/wp-content/uploads/2019/02/shutterstock_654212890-blackwhite-1080x675.jpg" style="
    width: 100%;
    height: 100%;
    border-radius: 7px;
    object-fit: contain;
">
            
            
            
        </div>
</div>
    <div class="two" style="flex: 1;">
	<div class="box_2" style="width: 80%;margin-left: 10%;margin-right: 10%;">
            <h2 style="
    font-size: 30px;
    font-weight: bold;
">Qui sommes-nous ?</h2>
            <p style="
    text-align: center;
">G2D, Go to Digital, est une start up composée de 5 personnes qui propose des services pour déployer des solutions techniques adaptées aux besoins des clients.</p>
            
        </div>
</div>
</div><div class="header" style="text-align: center;">
            <h2 id="title" style="font-size: 40px;">Notre produit : AutoMobile</h2>
        </div><div class="container" style="display: flex;height: auto;margin-bottom: 5%;">
    <div class="one" style="flex: 0 0 65%;">
     <div class="box_2" style="margin-left: 15%; width: 80%;">
            <h2 style="font-size: 30px;font-weight: bold;">Qu'est-ce que c'est ?</h2>
            <p style="text-align: center;">Nous vous proposons “ AutoMobile”,  un boitier de tests qui vise à évaluer l’aptitude à conduire des personnes âgées et des personnes ayant étant accidentées (ayant subi un traumatisme crânien).
</p>
<p style="text-align: center;">
Le produit permet donc de faire passer des tests psychotechniques aux personnes mentionnées plus précédemment. L’objectif de ce boitier est d’être le plus simple d’utilisation et le plus nomade possible, afin de permettre aux centres d’examens tels que les auto-écoles, de ne pas avoir à engager de moyens humains supplémentaires pour faire passer ces tests. 
</p>

</div>
</div>

    <div class="two" style="flex: 1;">
	<div class="box_2" style="
    width: auto;
    height: auto;
    box-shadow: 0px 0px 14px 3px #cfcfcf;
    margin-left: 15%;
    margin-right: 15%;
    margin-top: 5%;
">
    
    <img src="./public/img/Product_logo.jpg" style="
    width: 100%;
    height: 100%;
    border-radius: 7px;
    object-fit: contain;
">
            
            
            
</div></div>
</div>
<div class="header" style="
    text-align: center;
">
            <h2 id="title" style="
    font-size: 40px;
">A propos du site</h2>
        </div><div class="container" style="display: flex;height: auto;margin-bottom: 5%;">
    

    <div style="flex: 1;">
	<div class="box_2" style="margin-left: 10%; width: 80%;">
            
            <p style="text-align: center">
    Ce site vous permet d’utiliser ce produit en vous identifiant ou en créant un compte dans la rubrique <a href="/views/loginForm.php">CONNEXION</a>.
</p>
<p style="text-align: center">
    Vous pouvez également participer au <a href="/controllers/forumPage.php">FORUM</a> ou trouver des réponses à vos questions dans la rubrique <a href="/views/faqHome.php">FAQ</a>.
</p>
<p style="text-align: center">
Pour finir, si vous souhaitez nous contacter, vous trouverez toutes les informations nécessaires dans la rubrique SAV.
</p>


</div></div>
</div>


<?php 
$content = ob_get_clean(); 
require('./views/template.php'); 
?>

