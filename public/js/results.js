$(document).ready(function()
{
    console.log("JS chargé");

    var acc = document.getElementsByClassName("accordion_results");
    var i;

    for (i = 0; i < acc.length; i++) {
        acc[i].addEventListener("click", function() {
            this.classList.toggle("active");
               
            var panel = $(".panel").get($(".accordion_results").index(this));
            if (panel.style.display === "block") {
                panel.style.display = "none";
                $(this).css("color","black");
                $(this).css("background-color","#ebebeb");
                $(this).css("border","1px solid black");
            } else {
                panel.style.display = "block";
                $(this).css("color","#ebebeb");
                $(this).css("background-color","#606060");
                $(this).css("border","1px solid #ebebeb");
            }
        });
    }

});