$(document).ready(function()
{
    console.log("JS chargé");

    /** Changer couleur du texte onHover */
    $(".label").mouseenter(function(){
        $(this).find('h1').css('color','#d0c7a3');
        $(this).find('p').css('color','#d0c7a3');
    });

    $(".label").mouseleave(function(){
        $(this).find('h1').css('color','#000000');
        $(this).find('p').css('color','#000000');
    });

});