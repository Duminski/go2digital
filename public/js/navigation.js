$(document).ready(function()
{
	console.log("Nav js loaded");
	
	var height = $(window).height();
	updateAnchor();
	activeColor();

	// Change l'élément actif de la navbar
	$('nav li a').mousedown(function()
	{
		if($(this).attr('href') == "/#products"){
			$(".arrow").click()
		}
		activeColor();
	});

	// Smooth Scroll arrow
	$(".arrow").click(function() {
		$('html,body').animate({
			scrollTop: $("#products").offset().top - 50},
			1000, 'easeInOutQuint')
			.delay(600)
			.animate(
			  {height: "show"},
			  9000,
			  'easeInOutQuint'
			);
	});

	$("#sav").click(function(e) {
		e.preventDefault();
		if ($(".contact-container").css("display") == "none") {
			$(".contact-container").css("display","block");
		} else {
			$(".contact-container").css("display","none");
		}
	});

	// Change l'opacité de la navbar onScroll
	window.onscroll = function (){
		var navScroll = document.querySelector("nav ul");
		
		if(window.pageYOffset>=40){
			$(navScroll).css("background-color","rgba(255,255,255,1)");
			$(navScroll).css("transition", "0.6s");

			$(navScroll).css("box-shadow","0px 5px 5px 0px rgba(184, 184, 184,1)");
			$(navScroll).css("-moz-box-shadow","0px 5px 5px 0px rgba(184, 184, 184,1)");
			$(navScroll).css("-webkit-box-shadow","0px 5px 5px 0px rgba(184, 184, 184,1)");
		}
		else {
			$(navScroll).css("background-color","rgba(255,255,255,0)");
			$(navScroll).css("transition", "0.6");

			$(navScroll).css("box-shadow","0px 5px 5px 0px rgba(184, 184, 184,0)");
			$(navScroll).css("-moz-box-shadow","0px 5px 5px 0px rgba(184, 184, 184,0)");
			$(navScroll).css("-webkit-box-shadow","0px 5px 5px 0px rgba(184, 184, 184,0)");
		}

		updateAnchor();
	};

	// Actualise l'anchor de la page pour la homepage
	function updateAnchor(){
		var hash;
		var currPos = window.pageYOffset;
		var path = window.location.pathname;
		
		
		if(path == "/"){
			// En fonction de la position : récupère le bon hash
			if(currPos <= height) {
				hash = "#home";
				window.history.pushState('home', 'G2D', hash);
				if (path == "/") isActiveNavbar($("a"), $('a[href="' + path + hash + '"]'));
				else isActiveNavbar($("a"), $('a[href="' + path + '"]'));				
			}
			else if(currPos > height && currPos <= height*2) {
				hash = "#products";
				window.history.pushState('products', 'G2D', hash);
				if (path == "/") isActiveNavbar($("a"), $('a[href="' + path + hash + '"]'));
				else isActiveNavbar($("a"), $('a[href="' + path + '"]'));		
			}
		}
	};

	// Change l'état des items de la navbar
	function isActiveNavbar(currLink, newLink){
		currLink.removeClass("active");
        newLink.addClass("active");
	}

	// Change l'état des items de la navbar
	function activeColor(){
		$('a').removeClass('active');
		
		if(window.location.hash){
			$('a[href="/' + window.location.hash + '"]').addClass("active");
		}
		else {
			$('a[href="' + window.location.pathname + '"]').addClass("active");
		}		
	}

});