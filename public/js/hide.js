$(document).ready(function() {
    
    $(window).resize(function() {
        if($(window).width() >= 900) {
            $('#footer').show();
        } else {
            $('#footer').hide();
        }
    }).resize();
});