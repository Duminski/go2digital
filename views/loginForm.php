<?php 
$title = 'Espace personnel';
$linkCss = "/public/css/main.css";
$linkCss2 = "/public/css/login.css";
$linkIcon = "/public/img/logo.ico";
$linkLogo = "/public/img/logo_company.png";
$linkScript = "/public/js/navigation.js";
$linkScript2 = "/public/js/login.js";
$linkScript3 = "/public/js/hide.js";
ob_start(); 
?>
<?php
if(!isset($_COOKIE["auth_session"])){
?>
    <div class="container">
        <img src="/public/img/back_login.jpg" alt="background_landscape">
        <div class="card">
            <h1 class="title">Connexion</h1>
            <form method="post" action="/controllers/profilePage.php">
                <div class="input-container"><input type="text" name="username" placeholder="Pseudo" required="required" />
                    <div class="bar"></div>
                </div>
                <div class="input-container"><input type="password" name="password" placeholder="Mot de passe" required="required" />
                    <div class="bar"></div>
                </div>
                <div class="button-container">
                    <input class="button" type="submit" value="Valider">
                </div>
                <div class="footerLogin">
                    <a href="#">Mot de passe oublié ?</a></br>
                    <a href="/views/signUp.php">Pas encore de compte ?</a>
                </div>
            </form>
        </div>
    </div>
<?php
}
else echo 'ok';
?>
<?php 
$content = ob_get_clean(); 
require('template.php'); 
?>