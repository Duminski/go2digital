<?php 
$title = 'Forum G2D';
$linkCss = "/public/css/main.css";
$linkCss2 = "/public/css/login.css";
$linkCss3 = "/public/css/forum.css";
$linkIcon = "/public/img/logo.ico";
$linkLogo = "/public/img/logo_company.png";
$linkScript = "/public/js/navigation.js";
$linkScript2 = "/public/js/login.js";
$linkScript3 = "/public/js/forum.js";
ob_start(); 
?>

    <div class="content">
        <div class="header">
            <h1 id="title">Bienvenue sur le forum</h1>
        </div>
        
        <div class="boxes">
            <?php
            foreach($topicsToHtml as $val)
            {
            ?>
                <div class="topic">
                    <div class="label">
                        <h1 class="title"><?= $val['title'] ?></h1>
                        <p class="desc"><?= $val['desc'] ?></p>
                    </div>

                    <div class="v-bar"></div>

                    <div class="details">
                        <div class="row">
                            <h2 class="type">Posts</h2>
                            <h2 class="type">Crée par</h2>  
                            <h2 class="type">Activité</h2>  
                        </div>
                        <div class="row">                    
                            <p class="type"><?= $val['nbPosts'] ?></p>
                            <p class="type"><?= $val['author'] ?></p>
                            <p class="type"><?= $val['updated'] ?></p>
                        </div>
                    </div>
                </div>

            <?php
            }
            ?>

        </div>
    </div>

<?php 
$content = ob_get_clean(); 
    require('template.php'); 
?>
