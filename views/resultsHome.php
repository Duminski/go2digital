<?php 
$title = 'Forum G2D';
$linkCss = "/public/css/main.css";
$linkCss2 = "/public/css/login.css";
$linkCss3 = "/public/css/results.css";
$linkIcon = "/public/img/logo.ico";
$linkLogo = "/public/img/logo_company.png";
$linkScript = "/public/js/navigation.js";
$linkScript2 = "/public/js/login.js";
$linkScript3 = "/public/js/results.js";
ob_start(); 
?>

    <div class="content">
        <div class="header" style="margin-bottom: 40px;">
            <h1 id="title">Les résultats de vos tests</h1>
        </div>
        <div id="nbTests">
            <div id="effectue">
                TESTS EFFECTUES : 
                <span>2</span>
            </div>
            <div id="planifie">
                TESTS PLANIFIES : 
                <span>1</span>
            </div>
        </div>

        <br>
        <h3 style="text-align: left; margin-left: 10%;">VOS TESTS</h3>
        <div class="panel_body">
            <div class="panel_header">
                <div class="left_part">
                    Test 1 - Jugé <span class="aptitude">apte</span> à conduire
                </div>
                <div class="right_part">
                    <div class="date_part">
                        <span>Effectué le :</span><span class="date">13 août 2018</span>
                    </div>
                    <button class="accordion_results">Détails</button>
                </div>
            </div>
            <div class="panel">
                <h2 class="score">Votre score : <span>65</span></h2>
                <h4 class="score_ref">(score de référence : <span>50</span>)</h4>

                <br>
                <div class="competences">
                    <table class="tab_comp">
                        <tr><th>Compétence</th><th>Evaluation</th></tr>
                        <tr class="evenmt_prev"><td class="titre">Capacité à réagir face à un évènement prévisible</td><td class="eval">Apte</td></tr>
                        <tr class="evenmt_imprev"><td class="titre">Capacité à réagir face à un évènement imprévisible</td><td class="eval">Apte</td></tr>
                        <tr class="environmt_clair"><td class="titre">Capacité à voir dans un environnement clair (lumière du jour)</td><td class="eval">Apte</td></tr>
                        <tr class="environmt_obscur"><td class="titre">Capacité à voir dans un environnement obscure (nuit)</td><td class="eval">Apte</td></tr>
                    </table>
                    <br>
                    <hr>
                    <h2>Le détail de vos compétences</h2>
                    <table class="detail_comp">
                        <tr>
                            <td><a href="#clair"><img title="Environnement clair" src="/public/img/environnement_clair" alt="img_clair"></a></td>
                            <td><a href="#previsible"><img title="Evènement prévisible" src="/public/img/evenement_previsible" alt="img_prev"></a></td>
                        </tr>
                        <tr>
                            <td><a href="#obscure"><img title="Environnement obscure" src="/public/img/environnement_obscure" alt="img_obscure"></a></td>
                            <td><a href="#imprevisible"><img title="Evènement imprévisible" src="/public/img/evenement_imprevisible" alt="img_imprev"></a></td>
                        </tr>
                    </table>
                    <h4>*cliquer sur l'icône de la compétence dont vous voulez connaître le détail</h4>
                    <br>
                </div>
            </div>
        </div>
        <div class="panel_body">
            <div class="panel_header">
                <div class="left_part">
                    Test 2 - Jugé <span class="aptitude">apte</span> à conduire
                </div>
                <div class="right_part">
                    <div class="date_part">
                        <span>Effectué le :</span><span class="date">22 janvier 2019</span>
                    </div>
                    <button class="accordion_results">Détails</button>
                </div>
            </div>
            <div class="panel">
                <h2 class="score">Votre score : <span>80</span></h2>
                <h4 class="score_ref">(score de référence : <span>50</span>)</h4>

                <br>
                <div class="competences">
                    <table class="tab_comp">
                        <tr><th>Compétence</th><th>Evaluation</th></tr>
                        <tr class="evenmt_prev"><td class="titre">Capacité à réagir face à un évènement prévisible</td><td class="eval">Apte</td></tr>
                        <tr class="evenmt_imprev"><td class="titre">Capacité à réagir face à un évènement imprévisible</td><td class="eval">Apte</td></tr>
                        <tr class="environmt_clair"><td class="titre">Capacité à voir dans un environnement clair (lumière du jour)</td><td class="eval">Apte</td></tr>
                        <tr class="environmt_obscur"><td class="titre">Capacité à voir dans un environnement obscure (nuit)</td><td class="eval">Apte</td></tr>
                    </table>
                    <br>
                    <hr>
                    <h2>Le détail de vos compétences</h2>
                    <table class="detail_comp">
                        <tr>
                            <td><a href="#clair"><img title="Environnement clair" src="/public/img/environnement_clair" alt="img_clair"></a></td>
                            <td><a href="#previsible"><img title="Evènement prévisible" src="/public/img/evenement_previsible" alt="img_prev"></a></td>
                        </tr>
                        <tr>
                            <td><a href="#obscure"><img title="Environnement obscure" src="/public/img/environnement_obscure" alt="img_obscure"></a></td>
                            <td><a href="#imprevisible"><img title="Evènement imprévisible" src="/public/img/evenement_imprevisible" alt="img_imprev"></a></td>
                        </tr>
                    </table>
                    <h4>*cliquer sur l'icône de la compétence dont vous voulez connaître le détail</h4>
                    <br>
                </div>
            </div>
        </div>
    
    </div>

<?php 
$content = ob_get_clean(); 
require('template.php'); 
?>
