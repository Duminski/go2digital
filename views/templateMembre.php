<?php	$page = $_SERVER['PHP_SELF']; ?>
<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Strict//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd'>
<html xmlns='http://www.w3.org/1999/xhtml' xml:lang='fr' lang='fr'>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title><?= $title ?></title>
        <link rel='STYLESHEET' type='text/css' href=<?= $linkCss ?> />	
        <link rel='STYLESHEET' type='text/css' href=<?= $linkCss2 ?> />	
        <link rel='STYLESHEET' type='text/css' href=<?= $linkCss3 ?> />	
        <link rel="icon" href=<?= $linkIcon ?> />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
        <script src=<?= $linkScript ?>></script>
        <script src=<?= $linkScript2 ?>></script>
        <script src=<?= $linkScript3 ?>></script>
    </head>
    <body>
        <div class="page">
            <!--        Barre de navigation (NAVBAR)          -->
            <div class="box-shadow">
                <nav class="stroke">
                    <label for="hamburger">&#9776;</label>
                    <input type="checkbox" id="hamburger"/>
                    <img src=<?= $linkLogo ?> alt="Logo G2D">
                    <ul>
                        <li><a href="/#home">Accueil</a></li>
                        <li><a href="/views/resultsHome.php">R&eacute;sultats</a></li>
                        <li><a href="/views/faqHome.php">FAQ</a></li>
                        <li><a href="/controllers/forumPage.php">Forum</a></li>
                        <li id="sav"><a href="">SAV</a></li>
                        <li><a href="/controllers/profilePage.php">Espace personnel</a></li>
                        <li><a href="/controllers/signedOut.php">D&eacute;connexion</a></li>
                    </ul>
                </nav>
            </div>  

            <div class="contact-container" style="border: 3px solid black;">

                <form method="post">
                    <h4 id="titre">Contactez-nous</h4>
                    
                    <p>Indiquez votre demande dans le formulaire ci-dessous et nous vous r�pondrons d�s que possible.</p>

                    <label for="objet">Objet du message</label>
                    <input type="text" id="object" name="objet" placeholder="Objet de la demande" required="required">
                    <label for="sujet">Texte du message</label>
                    <textarea id="subject" name="sujet" placeholder="Entrez le texte de votre message..." required="required" style="height:200px"></textarea>

                    <input type="submit" value="Submit">
                </form>
            </div>

            <?= $content ?>

            <!--        Barre de liens/contact (FOOTER)          -->
            <div id="footer">
                <ul class="categories">
                    <div class="links">
                        <li><a href="/#home" >Accueil</a></li>
                        <li><a href="/#products" >Produits</a></li>
                        <li><a href="/views/login_form.php">Connexion</a></li>
                        <li><a href="/#home">FAQ</a></li>
                        <li><a href="/controllers/forumPage.php">Forum</a></li>
                        <li><a href="/#home">SAV</a></li>
                        <li><a href="/#home">Terms</a></li>
                        <li><a href="/#home">Privacy</a></li>
                        
                        <div class="contact"><a href="mailto:go2digitalcorp@gmail.com"><i class="fa fa-envelope-o"></i></a></div>

                    </div>
                    
                    <!--<div class="sublists">
                        <h4>Aide</h4>
                        <li><a href="/#home" >Accueil</a></li>
                        <li><a href="/#products" >Produits</a></li>
                        <li><a href="/#home" >Espace client</a></li>
                        <li><a href="/#home" >Espace client</a></li>
                    </div>
                        
                    <div class="sublists">
                        <h4>� propos</h4>
                        <li><a href="/#home" >Accueil</a></li>
                        <li><a href="/#products" >Produits</a></li>
                        <li><a href="/#home" >Espace client</a></li>
                        <li><a href="/#home" >Espace client</a></li>
                    </div>-->
                </ul>
                <div class="bottom"><i class="fa fa-copyright"></i> Copyright 2019, Go2Digital <br/>Cr&eacute;&eacute; par Laura DUMIN, Erwan HINGANT, Baptiste BONNAUDET, Martin LE GOFF, Salma MRASSI et Robin POLLET.</div>
            </div>
        </div>
    </body>
</html>