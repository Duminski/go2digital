<?php 
$title = 'FAQ G2D';
$linkCss = "/public/css/main.css";
$linkCss2 = "/public/css/faq.css";
$linkIcon = "/public/img/logo.ico";
$linkLogo = "/public/img/logo_company.png";
$linkScript = "/public/js/navigation.js";
ob_start(); 
?>

<style>
.accordion_faq {
    background-color: #eee;
    color: #444;
    cursor: pointer;
    padding: 18px;
    width: 70%;
    border: none;
    text-align: left;
    outline: none;
    font-size: 15px;
    transition: 0.4s;
    margin-left: 15%;
    margin-right: 15%;
    margin-top: 1%;
}

.accordion_faq:hover {
    background-color: #ccc; 
}

.panel {
    padding: 0 18px;
    display: none;
    background-color: white;
    overflow: hidden;
    margin-left: 15%;
    margin-right: 15%;
    width: 65%;
    text-align: center;
    border: 1px solid grey;
}
</style>
            
<div class="content">
    <div class="header">
        <h1 id="title">Foire aux questions</h1>
    </div>

    <button class="accordion_faq">Puis-je revendre mon boitier Auto Mobile à un autre centre d'examen ?</button>
    <div class="panel">
        <p>Ce cas de figure a été prévu ! Si vous devez vous séparer d'un boitier, vous devez vous rendre dans votre espace personnel, puis aller dans la section "Mes produits" et 
        supprimer le boitier correspondant de la liste de vos produits. Le boitier ne sera donc plus associé à votre centre d'examen et pourra être associer à un autre compte.</p>
    </div>

    <button class="accordion_faq">Comment m'inscrire en tant que candidat ?</button>
    <div class="panel">
        <p>Pour vous inscrire en tant que candidat, vous devez avoir passé (ou avoir prévu) un test à l'aide du boitier Auto Mobile dans un des centres d'examen en possédant, c'est ce
        centre d'examen qui va créer votre compte pour vous et vous recevrez un mail de la part du service Auto Mobile à l'adresse que vous aurez donné au centre d'examen. Vous pourrez
        alors suivre le lien contenu dans le mail et procéder à la création de votre compte candidat.</p>
    </div>

    <button class="accordion_faq">Comment consulter mes résultats ?</button>
    <div class="panel">
        <p>C'est très facile ! Rendez vous sur l'espace de connexion et saisissez vos identifiants afin d'accéder à votre espace candidat. Si ce dernier n'a pas encore été créé, vous pouvez créer
        vos identifiants de connexion à partir du lien transmis par mail de la part du service Auto Mobile avant votre examen.</p>
    </div>
</div>

<script>
    var acc = document.getElementsByClassName("accordion_faq");
    var i;

    for (i = 0; i < acc.length; i++) {
    acc[i].addEventListener("click", function() {
        this.classList.toggle("active");
        var panel = this.nextElementSibling;
        if (panel.style.display === "block") {
            panel.style.display = "none";
        } else {
            panel.style.display = "block";
        }
    });
    }
</script>

<?php 
$content = ob_get_clean(); 
require('template.php'); 
?>
