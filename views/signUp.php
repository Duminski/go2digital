<?php 
$title = 'Espace personnel';
$linkCss = "/public/css/main.css";
$linkCss2 = "/public/css/login.css";
$linkIcon = "/public/img/logo.ico";
$linkLogo = "/public/img/logo_company.png";
$linkScript = "/public/js/navigation.js";
$linkScript2 = "/public/js/login.js";
ob_start(); 
?>
<?php
if(!isset($_COOKIE["auth_session"])){
?>
    <div class="container">
        <img src="/public/img/back_login.jpg" alt="background_landscape">
        <div class="card" style="height: 85vh">
            <h1 class="title">Inscription</h1>
            <form method="post" action="/controllers/signedUp.php">
                <div class="input-container"><input type="text" name="numero" placeholder="Numéro de série" required="required" />
                    <div class="bar"></div>
                </div>
                <div class="input-container"><input type="email" name="email" placeholder="Adresse mail" required="required" />
                    <div class="bar"></div>
                </div>
                <div class="input-container"><input type="text" name="username" placeholder="Pseudo" required="required" />
                    <div class="bar"></div>
                </div>
                <div class="input-container"><input type="password" name="password" placeholder="Mot de passe" required="required" />
                    <div class="bar"></div>
                </div>
                <div class="input-container"><input type="password" name="confirmPassword" placeholder="Confirmation du mot de passe" required="required" />
                    <div class="bar"></div>
                </div>
                <div class="button-container">
                    <input class="button" type="submit" value="Valider">
                </div>
                
            </form>
        </div>
    </div>
<?php
}
else echo 'ok';
?>
<?php 
$content = ob_get_clean(); 
require('./template.php'); 
?>