<?php 
$title = 'ProfilEdition';
$linkCss = "../public/css/main.css";
$linkCss2 = "../public/css/profile.css";
$linkIcon = "../public/img/logo.ico";
$linkLogo = "../public/img/logo_company.png";
$linkScript = "../public/js/navigation.js";
ob_start(); 
?>

    <div class="content">
        <div class="header">
            <h1 id="title">Edition du profil</h1>
            <p>Vous pouvez changer vos informations de profil ici !</p>
        </div>
        
        <?php
        echo $userInfo;
        ?>

        <p>Nom :</p>
        <div class="input-container"><input type="name" name="name" placeholder="Nom" />
            <div class="bar"></div>
        </div>
        <p>Pr&eacute;nom :</p>
        <div class="input-container"><input type="text" name="surname" placeholder="Pr&eacute;nom" />
            <div class="bar"></div>
        </div>
        <p>Email :</p>
        <div class="input-container"><input type="email" name="email" placeholder="Adresse mail" />
            <div class="bar"></div>
        </div>
        <p>Pseudo (il remplacera votre nom sur le forum) :</p>
        <div class="input-container"><input type="text" name="username" placeholder="Pseudo" />
            <div class="bar"></div>
        </div>
        <p>Mot de passe :</p>
        <div class="input-container"><input type="text" name="password" placeholder="Mot de passe actuel" />
            <div class="bar"></div>
        </div>
        <div class="input-container"><input type="text" name="password" placeholder="Nouveau mot de passe" />
            <div class="bar"></div>
        </div>
        <div class="input-container"><input type="text" name="password" placeholder="Nouveau mot de passe" />
            <div class="bar"></div>
        </div>
        <div class="button-container">
            <input class="button" type="submit" value="Valider">
        </div>
    </div>
        
<?php 
$content = ob_get_clean(); 
require('template.php'); 
?>
