<?php 
$title = 'Profil';
$linkCss = "../public/css/main.css";
$linkCss2 = "../public/css/profile.css";
$linkCss3 = "";
$linkIcon = "../public/img/logo.ico";
$linkLogo = "../public/img/logo_company.png";
$linkScript = "../public/js/navigation.js";
ob_start(); 
?>

<div class="container" id="home_userpage">
    <img src="../public/img/landscape4.jpg" alt="background_landscape">
    <h2 class="moto">Bienvenue dans votre espace personnel</h2>
</div>
<div class="header" id="products" style="text-align: center;">
</div>

    
<div class="container" style="display: flex;height: auto;margin-bottom: 5%; flex-direction: row;background: #FFFFFF;">
    <div class="one" style="flex: 0 0 25%;">
    	<br />
	    <div class="box_2" style=" width: 300px; height: 300px; box-shadow: 0px 0px 14px 3px #cfcfcf; margin-left: 25%;">
	    
	    	<img src="../public/img/checklist_icon.png" style="width: 100%; height: 100%; border-radius: 7px; object-fit: contain;">

	    </div>
	</div>
    <div class="two" style="flex: 1;">
		<div class="box_2" style="width: 80%;margin-left: 10%;margin-right: 10%;">
	        <h2 style="font-size: 30px;font-weight: bold;">Que pouvez-vous faire ici ?</h2>
	        <p style="text-align: center;">Vous pouvez accéder aux résultats de tous vos test effectués. Pour cela, cliquer ici : <div class="button-container"><a href="../views/resultsHome.php"><input class="button" type="button" value="Mes r&eacute;sultats"></a></div></p>
	        <p style="text-align: center;">Si vous souhaitez échanger avec d'autres utilisateurs à propos des test effectués, vous pouvez utiliser le forum. Pour cela, cliquer ici : <div class="button-container"><a href="../controllers/forumPage.php"><input class="button" type="button" value="Forum"></a></div></p>
	        <p style="text-align: center;">Pour modifier vos informations personnelles ou votre mot de passe, cliquez ici : <div class="button-container"><a href="../views/profileEdition.php"><input class="button" type="button" value="Mon profil"></a></div></p>
	    </div>
	</div>
</div>
 
<?php 
$content = ob_get_clean(); 
require('templateMembre.php'); 
?>