<?php 
$title = 'Espace personnel';
$linkCss = "/public/css/main.css";
$linkCss2 = "/public/css/login.css";
$linkIcon = "/public/img/logo.ico";
$linkLogo = "/public/img/logo_company.png";
$linkScript = "/public/js/navigation.js";
$linkScript2 = "/public/js/login.js";
ob_start(); 
?>
<?php
if(!isset($_COOKIE["auth_session"])){
?>
    <div class="container">
        <img src="/public/img/back_login.jpg" alt="background_landscape">
        <div class="card" style="height: 80vh">
            <h1 class="title">Inscription d'un candidat</h1>
            <form method="post" action="/controllers/registeredCandidat.php">
                <div class="input-container"><input type="text" name="nom" placeholder="Nom" required="required" />
                    <div class="bar"></div>
                </div>
                <div class="input-container"><input type="text" name="prenom" placeholder="Pr&eacute;nom" required="required" />
                    <div class="bar"></div>
                </div>
                <div class="input-container"><input type="email" name="email" placeholder="Adresse mail" required="required" />
                    <div class="bar"></div>
                </div>
                <div class="button-container">
                    <input class="button" type="submit" value="Enregistrer">
                </div>
            </form>
        </div>
    </div>
<?php
}
else echo 'ok';
?>
<?php 
$content = ob_get_clean(); 
require('./template.php'); 
?>